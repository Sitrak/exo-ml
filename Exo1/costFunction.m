function J = costFunction(x,y,theta)

m = length(x);

J = 1/(2*m) * (x*theta - y)'*(x*theta - y);
end