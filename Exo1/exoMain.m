donne = load("ex1data1.txt");

x = donne(:,1);
y = donne(:,2);

plot(x,y,'o');

x = [ones(length(x),1),x];

theta = zeros(2,1);

costFunction(x,y,theta)

nbr_iter = 1500;
alpha = 0.01;

[theta,Jvals] = gradientDescent(alpha,nbr_iter,theta,x,y);
 plot(Jvals);
 
 predict1 = [1, 4.5] * theta;