function [theta,Jvals] = gradientDescent(alpha,nbr_iter,theta_init,x,y)

Jvals = [];

m = length(x);

theta = theta_init;

for i = 1:nbr_iter
theta = theta - (alpha/m) * (x' * (x * theta  - y));
Jvals(i) = costFunction(x,y,theta);
end

end