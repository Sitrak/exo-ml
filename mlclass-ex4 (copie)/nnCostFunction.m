function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
  %%NNCOSTFUNCTION Implements the neural network cost function for a two layer
  %%neural network which performs classification
  %%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
  %%   X, y, lambda) computes the cost and gradient of the neural network. The
  %%   parameters for the neural network are "unrolled" into the vector
  %%   nn_params and need to be converted back into the weight matrices.
  %%
  %%   The returned parameter grad should be a "unrolled" vector of the
  %%   partial derivatives of the neural network.
  %%

  %% Reshape nn_params back into the parameters Theta1 and Theta2, the
  %% weight matrices for our 2 layer neural network
  Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size +
                                                    1)), ...
                   hidden_layer_size, (input_layer_size + 1));

  Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                   num_labels, (hidden_layer_size + 1));

  %% Setup some useful variables
  m = size(X, 1);

  %% You need to return the following variables correctly
  J = 0;
  Theta1_grad = zeros(size(Theta1));
  Theta2_grad = zeros(size(Theta2));

  %% Forward propagation
  % Compute the final output h of theta
  a1 = [ones(m, 1) X];
  z2 = a1 * Theta1';
  a2 = [ones(m, 1) sigmoid(z2)];
  z3 = a2 * Theta2';
  hyp = sigmoid(z3);
  a3 = hyp;
  
  %% Compute the regularized cost function
  for k = 1:num_labels
    hyp_k = hyp(:,k);
    y_k = (y == k);
    
    J = J + (-y_k' * log(hyp_k) - (1 - y_k)' * log(1 - hyp_k)) * 1/m;
  endfor
  
  
  [n_t1,m_t1] = size(Theta1);
  t1 = 0;
  for i = 1: n_t1 
   for j = 2: m_t1
     t1 = t1 + Theta1(i,j)^2 ;
   endfor
  endfor
  
  [n_t2,m_t2] = size(Theta2);
  t2 = 0;
  for k = 1: n_t2
   for l = 2: m_t2
     t2 = t2 + Theta2(k,l)^2 ;
   endfor
  endfor
  
  J = J + lambda/ (2 * m) * (t1 + t2);
  
  %% Back propagation
  
  %Store output value in matrix (10x5000)
  y_k = zeros(num_labels, m);

  for i=1:num_labels,
      y_k(i,:) = (y == i);
  endfor
  
  %size(ones(size(z2,2)),1)
  
  
  for k = 1: m
    %Forward propogation
    %act_1 = X(k,:);
   % z2 = Theta1 * act_1';
    
    %act_2 = [1 ; sigmoid(z2)];
    
   % act_3 = sigmoid(Theta2 * act_2);
    
    %Compute “error term” in layer 3
    delta_3 = hyp(k) - y(k);
    
    %compute “error term” in layer 2
    %z2 = [ones(1,size(z2,2)) ; z2];
    
    %z2 = [1; z2];
    %z2 = [1 ; z2]; %Add a bias
    %delta_2 = (Theta2' * delta_3) .* sigmoidGradient(z2);
    
    % Remove bias from delta_2
    %delta_2 = delta_2(2:end);
    
    %Theta1_grad = (Theta1_grad + delta_2 * act_1);
    %Theta2_grad = (Theta2_grad + delta_3 * a3');
  %endfor 
  %size(z2 = [ones(size(z2,1),1) z2])
  %Theta1_grad = Theta1_grad ./ m;
  %Theta2_grad = Theta2_grad ./ m;

  %Theta1_grad = delta_2./m + (lambda/m) * [zeros(size(Theta1, 1), 1) Theta1(:, 2:end)];
  %Theta2_grad = delta_3./m + (lambda/m) * [zeros(size(Theta2, 1), 1) Theta2(:, 2:end)];
    %[zeros(size(Theta1, 1), 1) Theta1(:, 2:end)]
  %% Unroll gradients
  grad = [Theta1_grad(:) ; Theta2_grad(:)];
end